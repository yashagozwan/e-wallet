import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

//* contexts
import { navigationRef } from '../navigations/RootNavigation';
import { Provider as AuthProvider } from '../contexts/AuthContext';
import { Provider as AccountProvider } from '../contexts/AccountContext';
import { Provider as WalletProvider } from '../contexts/WalletContext';

//* screens
import {
  AuthSigninScreen,
  AuthSignupScreen,
  AccountScreen,
  AuthLocalScreen,
  AccountSettingScreen,
  WalletSendScreen,
  WalletSendProccesScreen,
  WalletSendVerifyScreen,
  WalletTopupScreen,
  WalletHistoryScreen,
  WalletSendProofScreen,
  WalletSendErrorScreen,
  WalletHistoryDetailScreen,
  AccountSettingProfileScreen,
  AccountSettingPasswordScreen,
} from '../screens';

//* components
import { colors, Icon, IconBox } from '../components';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const AuthFlow = () => (
  <Stack.Navigator screenOptions={{ headerShown: false }}>
    <Stack.Screen name="Signin" component={AuthSigninScreen} />
    <Stack.Screen name="Signup" component={AuthSignupScreen} />
  </Stack.Navigator>
);

const HomeFlow = () => (
  <Stack.Navigator screenOptions={{ headerShown: false }}>
    <Stack.Screen name="WalletTopup" component={WalletTopupScreen} />
    <Stack.Screen name="WalletHistory" component={WalletHistoryScreen} />
    <Stack.Screen
      name="WalletHistoryDetail"
      component={WalletHistoryDetailScreen}
    />
  </Stack.Navigator>
);

const WalletSendFlow = () => (
  <Stack.Navigator
    initialRouteName="WalletSend"
    screenOptions={{ headerShown: false }}
  >
    <Stack.Screen
      name="WalletSendProcces"
      component={WalletSendProccesScreen}
    />
    <Stack.Screen name="WalletSendVerify" component={WalletSendVerifyScreen} />
    <Stack.Screen name="WalletSendProof" component={WalletSendProofScreen} />
    <Stack.Screen name="WalletSendError" component={WalletSendErrorScreen} />
  </Stack.Navigator>
);

const AccountSettingsFlow = () => (
  <Stack.Navigator screenOptions={{ headerShown: false }}>
    <Stack.Screen
      name="ProfileSetting"
      component={AccountSettingProfileScreen}
    />
    <Stack.Screen
      name="PasswordSetting"
      component={AccountSettingPasswordScreen}
    />
  </Stack.Navigator>
);

const AccountFlow = () => {
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused }) => {
          let iconName;
          let color;

          switch (route.name) {
            case 'Account':
              iconName = focused ? 'home' : 'home-outline';
              color = focused ? colors.primary : colors.primaryPressed;
              break;
            case 'AccountSetting':
              iconName = focused ? 'settings' : 'settings-outline';
              color = focused ? colors.primary : colors.primaryPressed;
              break;
          }

          return (
            <Icon typeLibrary="ion" size={25} color={color} name={iconName} />
          );
        },
        headerShown: false,
        tabBarLabel: () => null,
      })}
    >
      <Tab.Screen name="Account" component={AccountScreen} />
      <Tab.Screen
        name="WalletSend"
        component={WalletSendScreen}
        options={() => ({
          tabBarIcon: ({ focused }) => {
            const iconName = focused ? 'send' : 'send-o';
            return <IconBox iconName={iconName} mb={20} />;
          },
        })}
      />
      <Tab.Screen name="AccountSetting" component={AccountSettingScreen} />
    </Tab.Navigator>
  );
};

const App = () => (
  <NavigationContainer ref={navigationRef}>
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="AuthLocal" component={AuthLocalScreen} />
      <Stack.Screen name="AuthFlow" component={AuthFlow} />
      <Stack.Screen name="AccountFlow" component={AccountFlow} />
      <Stack.Screen name="WalletSendFlow" component={WalletSendFlow} />
      <Stack.Screen name="HomeFlow" component={HomeFlow} />
      <Stack.Screen
        name="AccountSettingsFlow"
        component={AccountSettingsFlow}
      />
    </Stack.Navigator>
  </NavigationContainer>
);

export default () => {
  return (
    <WalletProvider>
      <AccountProvider>
        <AuthProvider>
          <App />
        </AuthProvider>
      </AccountProvider>
    </WalletProvider>
  );
};
