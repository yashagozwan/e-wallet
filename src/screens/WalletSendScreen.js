import React, { useEffect, useContext } from 'react';
import { StyleSheet, TextInput, View, TouchableOpacity } from 'react-native';

import { Context as WalletContext } from '../contexts/WalletContext';
import useNumber from '../hooks/useNumber';
import currencyFormat from '../utils/currencyFormat';
import {
  Screen,
  ScreenWithHeader,
  Text,
  Wrapper,
  Header,
  Spacer,
  colors,
  IconBox,
  Center,
  ViewAnimateTopToDown,
  List,
  Button,
  Icon,
} from '../components';

export default function WalletSendScreen({ navigation }) {
  const {
    state: {
      wallet: { accountNumber, balance, isActive },
      receiver: {
        name: receiverNameResult,
        accountNumber: accountNumberReceiverResult,
      },
      receiver404,
    },
    getReceiver,
    clearReceiver,
  } = useContext(WalletContext);
  const [visible, toggle] = React.useReducer((e) => !e, false);
  const [accountNumberReceiver, setAccountNumberReceiver] = useNumber();
  const [amount, setAmount] = useNumber();

  useEffect(() => {
    const onFocus = navigation.addListener('focus', () => {});
    const onBlur = navigation.addListener('blur', () => {
      setAccountNumberReceiver('');
      setAmount('');
      clearReceiver();
    });

    return () => {
      onFocus;
      onBlur;
    };
  }, [navigation]);

  return isActive ? (
    <ScreenWithHeader headerText="Kirim Uang" bgcolor={colors.white}>
      <List pv={16}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}
        >
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <IconBox
              typeLibrary="materialIcons"
              iconName="account-balance-wallet"
              mr={16}
              width={40}
              height={40}
              rounded={8}
            />
            <Text fontSize={15} fontWeight={600} color={colors.grey444}>
              Rp {visible ? (balance ? currencyFormat(balance) : 0) : '***'}
            </Text>
          </View>
          <TouchableOpacity onPress={toggle}>
            <Icon
              name={visible ? 'eye-slash' : 'eye'}
              typeLibrary="fontAwesome5"
              size={20}
              color={colors.primary}
            />
          </TouchableOpacity>
        </View>
      </List>

      <List pv={16}>
        <View style={styles.inputContainer}>
          <TextInput
            value={accountNumberReceiver}
            onChangeText={setAccountNumberReceiver}
            style={styles.inputStyle}
            placeholder="Masukan nomer tujuan"
            keyboardType="numeric"
            maxLength={8}
          />
          <TouchableOpacity
            disabled={accountNumberReceiver == accountNumber ? true : false}
            onPress={() => {
              getReceiver({
                accountNumber: accountNumberReceiver,
                amount,
              });
              setAmount('');
            }}
          >
            <IconBox
              typeLibrary="materialIcons"
              iconName={
                accountNumberReceiver == accountNumber
                  ? 'not-interested'
                  : 'search'
              }
              height={50}
              width={50}
              ml={8}
              iconSize={30}
              rounded={8}
            />
          </TouchableOpacity>
        </View>
      </List>

      {receiver404 ? (
        <ViewAnimateTopToDown>
          <List pv={16} style={{ backgroundColor: colors.red }}>
            <Text center color={colors.white}>
              {receiver404}
            </Text>
          </List>
        </ViewAnimateTopToDown>
      ) : null}

      {receiverNameResult ? (
        <ViewAnimateTopToDown>
          <List pv={0}>
            <Spacer />
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}
            >
              <View style={{ flexDirection: 'row' }}>
                <IconBox
                  typeLibrary="materialIcons"
                  iconName="person"
                  height={40}
                  width={40}
                  mr={16}
                  rounded={8}
                />
                <View>
                  <Text fontWeight={700} fontSize={16}>
                    {receiverNameResult}
                  </Text>
                  <Text fontWeight={500} fontSize={13}>
                    Rekening - {accountNumberReceiverResult}
                  </Text>
                </View>
              </View>
            </View>
          </List>

          <List pv={16}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}
            >
              <View
                style={{
                  flexDirection: 'row',
                  flex: 1,
                  borderWidth: 1,
                  borderColor: colors.primary,
                  paddingHorizontal: 16,
                  paddingVertical: 4,
                  borderRadius: 8,
                }}
              >
                <Text fontSize={16} style={{ alignSelf: 'center' }}>
                  Rp
                </Text>
                <TextInput
                  style={{ flex: 1, fontSize: 16 }}
                  keyboardType="numeric"
                  value={currencyFormat(parseInt(amount))}
                  onChangeText={
                    balance > 1000 ? (newAmount) => setAmount(newAmount) : null
                  }
                />
              </View>
              <TouchableOpacity
                onPress={() =>
                  navigation.navigate('WalletSendFlow', {
                    screen: 'WalletSendVerify',
                    params: {
                      name: receiverNameResult,
                      accountNumber: accountNumberReceiver,
                      amount: amount,
                    },
                  })
                }
              >
                <IconBox
                  typeLibrary="materialIcons"
                  iconName={
                    balance > amount && amount > 100
                      ? 'send'
                      : 'cancel-schedule-send'
                  }
                  boxColor={
                    balance > amount && amount > 100
                      ? colors.primary
                      : colors.red
                  }
                  ml={8}
                  iconSize={20}
                  width={40}
                  height={40}
                  rounded={8}
                />
              </TouchableOpacity>
            </View>
          </List>
        </ViewAnimateTopToDown>
      ) : null}

      {accountNumberReceiver == accountNumber ? (
        <ViewAnimateTopToDown>
          <List pv={16}>
            <Text center color={colors.red}>
              Tidak bisa mengirim ke rekening sendiri
            </Text>
          </List>
        </ViewAnimateTopToDown>
      ) : null}
    </ScreenWithHeader>
  ) : (
    <Screen full>
      <Wrapper>
        <Header headerText="Aktifkan Wallet" />
      </Wrapper>
      <Center>
        <IconBox
          typeLibrary="materialIcons"
          width={200}
          height={200}
          iconName="not-interested"
          iconSize={150}
          rounded={100}
        />
      </Center>
    </Screen>
  );
}

const styles = StyleSheet.create({
  inputContainer: {
    flexDirection: 'row',
  },
  inputStyle: {
    borderColor: colors.primary,
    borderWidth: 1,
    borderRadius: 8,
    paddingHorizontal: 16,
    fontSize: 16,
    flexGrow: 1,
    color: colors.grey444,
  },
});
