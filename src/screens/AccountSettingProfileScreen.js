import React, { useState } from 'react';
import { StyleSheet, View } from 'react-native';

import {
  Spacer,
  Screen,
  Text,
  Wrapper,
  Header,
  colors,
  List,
  Input,
  Button,
  Icon,
  CenterHorizontal,
} from '../components';

export default function AccountSettingProfileScreen({ navigation, route }) {
  const { name, email } = route.params;
  const [myName, setMyname] = useState(name);
  const [myEmail, setMyEmail] = useState(email);

  return (
    <Screen full bgColor={colors.primary} staturBarColor="light">
      <Wrapper style={{ height: 100 }}>
        <CenterHorizontal>
          <Icon name="user-circle-o" size={50} color={colors.white} />
        </CenterHorizontal>
      </Wrapper>
      <View style={styles.box}>
        <Text center fontWeight={600} fontSize={24}>
          Profile
        </Text>
        <Spacer bottom={32} />
        <Wrapper>
          <Input
            rounded={8}
            value={myName}
            onChangeText={setMyname}
            placeholder="Nama"
          />
          <Spacer bottom={8} />
          <Input
            rounded={8}
            value={myEmail}
            onChangeText={setMyEmail}
            placeholder="Email"
          />
          <Spacer bottom={32} />
          <Button rounded={8} title="Ubah" />
          <Spacer bottom={8} />
          <Button
            rounded={8}
            title="Kembali"
            onPress={() => navigation.pop()}
          />
        </Wrapper>
      </View>
    </Screen>
  );
}

const styles = StyleSheet.create({
  box: {
    flex: 1,
    backgroundColor: colors.white,
    paddingVertical: 16,
  },
});
