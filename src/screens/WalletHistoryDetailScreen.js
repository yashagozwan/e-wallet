import React from 'react';

import { StyleSheet, View } from 'react-native';

import {
  Screen,
  Text,
  Wrapper,
  Header,
  colors,
  Spacer,
  CardProof,
} from '../components';

export default function WalletHistoryDetailScreen({ route, navigation }) {
  const { receiverName, amount, receiverAccountNumber, createdAt } =
    route.params.item;

  return (
    <Screen
      full
      style={{ backgroundColor: colors.primary }}
      staturBarColor="light"
    >
      <Wrapper>
        <Text center fontSize={23} fontWeight={600} color={colors.white}>
          Bukti Transfer
        </Text>
      </Wrapper>
      <Spacer bottom={200} />
      <View
        style={{
          backgroundColor: colors.whiteBlue,
          flex: 1,
        }}
      >
        <View style={{ position: 'absolute', top: -160, right: 32, left: 32 }}>
          <CardProof
            name={receiverName}
            accountNumber={receiverAccountNumber}
            createdAt={createdAt}
            amount={amount}
          />
        </View>
      </View>
    </Screen>
  );
}
