import React, { useEffect, useContext, useState } from 'react';

import { Context as AuthContext } from '../contexts/AuthContext';
import {
  Text,
  Screen,
  Wrapper,
  Button,
  Input,
  Spacer,
  InputSecure,
  colors,
  TextLink,
  TextError,
} from '../components';

export default function AuthSigninScreen({ navigation }) {
  const {
    state: { errorMessage },
    signin,
    clearErrorMessage,
  } = useContext(AuthContext);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  return (
    <Screen full bgColor={colors.white}>
      <Spacer />
      <Wrapper>
        <Text center color={colors.primary} fontSize={24}>
          e-wallet
        </Text>
      </Wrapper>
      <Spacer bottom={32} />
      <Wrapper ph={32}>
        <Text fontWeight={600} fontSize={20}>
          Hallo!
        </Text>
        <Spacer bottom={8} />
        <Text fontSize={16} color="#444">
          Masukkan email dan password kamu
        </Text>
        <Spacer bottom={16} />

        <Input
          rounded={8}
          placeholder="Email"
          value={email}
          onChangeText={setEmail}
        />
        <Spacer bottom={8} />

        <InputSecure
          placeholder="Password"
          value={password}
          onChangeText={setPassword}
        />
        <Spacer bottom={16} />

        {errorMessage ? <TextError text={errorMessage} /> : null}

        <Button
          rounded={8}
          title="Masuk"
          upperCase
          onPress={() => signin({ email, password })}
        />
        <Spacer />
        <TextLink
          text="Belum punya akun e-walet? "
          buttonText="Daftar di sini"
          onPress={() => {
            navigation.push('Signup');
            clearErrorMessage();
          }}
        />
      </Wrapper>
    </Screen>
  );
}
