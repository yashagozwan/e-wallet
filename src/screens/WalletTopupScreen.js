import React from 'react';
import { StyleSheet } from 'react-native';

import { Screen, Text, Wrapper, Header } from '../components';

export default function WalletTopupScreen({ navigation }) {
  return (
    <Screen full>
      <Wrapper>
        <Header
          showBack
          headerText="Tambah Uang"
          onPress={() => navigation.pop()}
        />
      </Wrapper>
    </Screen>
  );
}

const styles = StyleSheet.create({});
