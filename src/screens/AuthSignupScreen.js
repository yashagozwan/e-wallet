import React, { useEffect, useContext, useState } from 'react';

import { Context as AuthContext } from '../contexts/AuthContext';
import useChar from '../hooks/useChar';
import {
  Text,
  Screen,
  Wrapper,
  Button,
  Input,
  Spacer,
  InputSecure,
  colors,
  TextLink,
  TextError,
} from '../components';

export default function AuthSignupScreen({ navigation }) {
  const {
    state: { errorMessage },
    signup,
    clearErrorMessage,
  } = useContext(AuthContext);

  // const [name, setName] = useState('');
  const [name, setName] = useChar();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  return (
    <Screen full bgColor={colors.white}>
      <Spacer />
      <Wrapper>
        <Text center color={colors.primary} fontSize={24}>
          e-wallet
        </Text>
      </Wrapper>
      <Spacer bottom={32} />
      <Wrapper ph={32}>
        <Text fontWeight={600} fontSize={20} mb={8}>
          Selamat Datang !
        </Text>
        <Text fontSize={16} color="#444">
          Lengkapi data berikut dan akun e-walletmu akan terbuat
        </Text>
        <Spacer bottom={16} />

        <Input
          rounded={8}
          placeholder="Nama Lengkap"
          value={name}
          onChangeText={setName}
        />
        <Spacer bottom={8} />

        <Input
          rounded={8}
          placeholder="Email"
          value={email}
          onChangeText={setEmail}
        />
        <Spacer bottom={8} />

        <InputSecure
          placeholder="Password"
          value={password}
          onChangeText={setPassword}
        />
        <Spacer bottom={16} />

        {errorMessage ? <TextError text={errorMessage} /> : null}

        <Button
          rounded={8}
          title="Daftar"
          upperCase
          onPress={() => signup({ name, email, password })}
        />
        <Spacer />

        <TextLink
          text="Sudah punya akun e-wallet? "
          buttonText="Masuk di sini"
          onPress={() => {
            navigation.push('Signin');
            clearErrorMessage();
          }}
        />
      </Wrapper>
    </Screen>
  );
}
