import React, { useState } from 'react';

import { StyleSheet, View, TextInput } from 'react-native';

import {
  colors,
  Header,
  IconBox,
  Screen,
  Spacer,
  Text,
  Wrapper,
  Button,
  CenterHorizontal,
} from '../components';
import currencyFormat from '../utils/currencyFormat';

export default function WalletSendVerifyScreen({ navigation, route }) {
  const { name, accountNumber, amount } = route.params;
  const [password, setPassword] = useState('');

  return (
    <Screen full>
      <Wrapper>
        <Header headerText="Verify" showBack onPress={() => navigation.pop()} />
        <Spacer />
      </Wrapper>

      <Wrapper>
        <View style={styles.cardVerify}>
          <Text center fontSize={24} mb={16}>
            {accountNumber}
          </Text>
          <CenterHorizontal>
            <IconBox
              typeLibrary="ion"
              iconName="person-circle-sharp"
              iconSize={70}
              width={100}
              height={100}
              rounded={50}
            />
          </CenterHorizontal>
          <Spacer />

          <Text center fontSize={18} fontWeight={600} mb={8}>
            {name}
          </Text>
          <Text center fontSize={16} fontWeight={600}>
            Rp{currencyFormat(parseInt(amount))}
          </Text>
          <Spacer bottom={8} />
          <Text center mb={8} fontWeight={500} fontSize={12}>
            Masukan Password
          </Text>
          <TextInput
            value={password}
            onChangeText={setPassword}
            autoCapitalize="none"
            autoCorrect={false}
            secureTextEntry
            style={styles.inputStyle}
          />
          <Spacer />
          <Button
            disabled={password ? false : true}
            rounded={8}
            title="Konfirmasi"
            onPress={() =>
              navigation.navigate('WalletSendProcces', {
                name,
                accountNumber,
                amount,
                password,
              })
            }
          />
        </View>
      </Wrapper>
    </Screen>
  );
}

const styles = StyleSheet.create({
  cardVerify: {
    backgroundColor: colors.white,
    padding: 16,
    borderRadius: 16,
  },
  inputStyle: {
    fontSize: 18,
    height: 50,
    width: '100%',
    textAlign: 'center',
    backgroundColor: colors.grey,
    borderRadius: 8,
  },
});
