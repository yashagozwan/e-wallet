import React, { useContext, useEffect } from 'react';
import { StyleSheet, View, TouchableOpacity } from 'react-native';

import { Context as AuthContext } from '../contexts/AuthContext';
import { Context as AccountContext } from '../contexts/AccountContext';
import { Context as WalletContext } from '../contexts/WalletContext';
import {
  colors,
  Screen,
  Spacer,
  Text,
  Wrapper,
  IconBox,
  ViewAnimate,
  VirtualCard,
  Loading,
  CardMenuList,
} from '../components';

export default function AccountScreen({ navigation }) {
  const { signout } = useContext(AuthContext);
  const {
    state: {
      account: {
        user: { name, email, createdAt },
      },
    },
    getAccount,
    clearAccount,
  } = useContext(AccountContext);

  const {
    state: {
      wallet: { accountNumber, balance, isActive },
    },
    getWallet,
    createWallet,
    clearWallet,
  } = useContext(WalletContext);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getAccount();
      getWallet();
    });

    return () => {
      unsubscribe;
    };
  }, [name, accountNumber]);

  if (!name) {
    return <Loading />;
  } else {
    return (
      <Screen full bgColor={colors.white}>
        <Wrapper>
          <View
            style={{ flexDirection: 'row', justifyContent: 'space-between' }}
          >
            <Text mb={8} fontSize={24} color={colors.primary} fontWeight={600}>
              e-wallet
            </Text>
            <TouchableOpacity
              onPress={() => {
                signout();
                clearAccount();
                clearWallet();
              }}
            >
              <IconBox
                typeLibrary="materialIcons"
                iconName="logout"
                width={40}
                height={40}
                rounded={50}
                iconSize={20}
              />
            </TouchableOpacity>
          </View>
          <Spacer />
        </Wrapper>

        <Wrapper>
          <Text color={colors.grey444} mb={8} fontWeight={600} fontSize={16}>
            Kartu Virtual
          </Text>
          {isActive ? (
            <ViewAnimate delay={100}>
              <VirtualCard
                name={name}
                accountNumber={accountNumber}
                balance={balance}
              />
            </ViewAnimate>
          ) : (
            <TouchableOpacity onPress={() => createWallet()}>
              <View style={styles.cardVisa}>
                <Text color={colors.white} fontSize={18} fontWeight={600}>
                  Aktifkan wallet
                </Text>
              </View>
            </TouchableOpacity>
          )}
          <Spacer />
        </Wrapper>

        <Wrapper>
          <Text color={colors.grey444} fontWeight={600} fontSize={16}>
            Menu
          </Text>
          <Spacer bottom={8} />
        </Wrapper>

        {isActive ? (
          <View>
            <ViewAnimate delay={200}>
              <CardMenuList
                boxColorIcon={colors.green}
                iconboxRounded={8}
                iconName="add"
                text="Tambah Uang"
                onPress={() =>
                  navigation.push('HomeFlow', { screen: 'WalletTopup' })
                }
              />
            </ViewAnimate>
            <ViewAnimate delay={300}>
              <CardMenuList
                typeLibrary="materialCommunity"
                iconName="history"
                iconboxRounded={8}
                text="Riwayat"
                onPress={() =>
                  navigation.push('HomeFlow', { screen: 'WalletHistory' })
                }
              />
            </ViewAnimate>
          </View>
        ) : null}
      </Screen>
    );
  }
}

const styles = StyleSheet.create({
  cardVisa: {
    backgroundColor: colors.primary,
    padding: 32,
    borderRadius: 8,
    marginRight: 8,
    width: 270,
  },
});
