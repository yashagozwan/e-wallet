import AsyncStorage from '@react-native-async-storage/async-storage';

import createDateContext from './createDataContext';
import yukieApi from '../apis/yukieApi';
import { navigate } from '../navigations/RootNavigation';

const SIGNIN = 'SIGNIN';
const SIGNUP = 'SIGNUP';
const SIGNOUT = 'SIGNOUT';
const SIGNIN_ERROR = 'SIGNIN_ERROR';
const SIGNUP_ERROR = 'SIGNUP_ERROR';
const CLEAR_ERROR_MESSAGE = 'CLEAT_ERROR_MESSAGE';

const initialState = {
  token: null,
  account: { name: null, email: null },
  errorMessage: null,
};

const authReducer = (state, action) => {
  switch (action.type) {
    case SIGNIN:
      return { token: action.payload, errorMessage: null };

    case SIGNUP_ERROR:
    case SIGNIN_ERROR:
      return { ...state, errorMessage: action.payload };

    case CLEAR_ERROR_MESSAGE:
      return initialState;

    case SIGNOUT:
      return {
        ...state,
        token: null,
        account: { name: null, email: null },
        errorMessage: null,
      };

    default:
      return state;
  }
};

const signup =
  (dispatch) =>
  async ({ name, email, password }) => {
    try {
      const { data } = await yukieApi.post('/api/auth/signup', {
        name,
        email,
        password,
      });
      if (data.success) {
        dispatch({ type: SIGNUP });
        navigate('AuthFlow', { screen: 'Signin' });
      }
    } catch (error) {
      dispatch({ type: SIGNUP_ERROR, payload: error.response.data.error });
    }
  };

const signin =
  (dispatch) =>
  async ({ email, password }) => {
    try {
      const { data } = await yukieApi.post('/api/auth/signin', {
        email,
        password,
      });
      if (data.success) {
        await AsyncStorage.setItem('token', data.token);
        dispatch({ type: SIGNIN, payload: data.token });
        navigate('AccountFlow', { screen: 'Account' });
      }
    } catch (error) {
      dispatch({ type: SIGNIN_ERROR, payload: error.response.data.error });
    }
  };

const signout = (dispatch) => async () => {
  await AsyncStorage.removeItem('token');
  navigate('AuthFlow', { screen: 'Signin' });
};

const clearErrorMessage = (dispatch) => () => {
  dispatch({ type: CLEAR_ERROR_MESSAGE });
};

const tryLocalSignin = (dispatch) => async () => {
  const token = await AsyncStorage.getItem('token');
  if (token) {
    dispatch({ type: SIGNIN, payload: token });
    navigate('AccountFlow', { screen: 'Account' });
  } else {
    navigate('AuthFlow', { screen: 'Signin' });
  }
};

export const { Context, Provider } = createDateContext(
  authReducer,
  {
    signin,
    signup,
    signout,
    clearErrorMessage,
    tryLocalSignin,
  },
  initialState
);
