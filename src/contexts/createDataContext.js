import React from 'react';

export default (reducer, actions, initialState) => {
  const Context = React.createContext();
  const Provider = ({ children }) => {
    const [state, dispatch] = React.useReducer(reducer, initialState);

    const bindActionCreators = {};
    for (let key in actions) {
      bindActionCreators[key] = actions[key](dispatch);
    }

    return (
      <Context.Provider value={{ state, ...bindActionCreators }}>
        {children}
      </Context.Provider>
    );
  };
  return { Context, Provider };
};
