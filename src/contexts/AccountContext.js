import createDataContext from './createDataContext';

import yukieApi from '../apis/yukieApi';

const GET_ACCOUNT = 'GET_ACCOUNT';
const CLEAR_ACCOUNT = 'CLEAT_ACCOUNT';

const accountReducer = (state, action) => {
  switch (action.type) {
    case GET_ACCOUNT:
      return {
        ...state,
        account: {
          ...state.account,
          role: action.payload.role,
          user: { ...state.account.user, ...action.payload.user },
        },
      };
    case CLEAR_ACCOUNT:
      return {
        ...state,
        account: {
          role: false,
          user: { name: null, email: null, createdAt: null },
        },
      };
    default:
      return state;
  }
};

//* METHOD GET
//* GET ACCOUNT
//* URL: /api/account
const getAccount = (dispatch) => async () => {
  try {
    const { data } = await yukieApi.get('/api/account');
    if (data.success) {
      dispatch({
        type: GET_ACCOUNT,
        payload: { role: data.account.role, user: data.account.user },
      });
    }
  } catch (error) {}
};

const clearAccount = (dispatch) => () => {
  dispatch({ type: CLEAR_ACCOUNT });
};

export const { Context, Provider } = createDataContext(
  accountReducer,
  { getAccount, clearAccount },
  {
    account: {
      role: false,
      user: { name: null, email: null, createdAt: null },
    },
  }
);
