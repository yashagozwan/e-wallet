import React, { useEffect, useState } from 'react';

const useChar = () => {
  const [char, setChar] = useState('');
  const [validChar, setValidChar] = useState('');

  useEffect(() => {
    const sanitize = (text) => {
      let init = /\w|\s/g;
      init = text.match(init);
      init = init ? init.join('') : '';
      console.log(init);
      return init;
    };

    setValidChar(sanitize(char));
  }, [char]);

  return [validChar, setChar];
};

export default useChar;
