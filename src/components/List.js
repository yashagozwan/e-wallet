import React from 'react';
import { View, StyleSheet } from 'react-native';

import Wrapper from './Wrapper';
import colors from './colors';

const List = ({ children, pv = 8, style }) => (
  <View style={[styles.list, { paddingVertical: pv }, style]}>
    <Wrapper>{children}</Wrapper>
  </View>
);

const styles = StyleSheet.create({
  list: {
    backgroundColor: colors.white,
    borderBottomColor: colors.primary,
    borderBottomWidth: 0.5,
  },
});

export default List;
