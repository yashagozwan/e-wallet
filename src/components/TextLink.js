import React from 'react';
import { TouchableOpacity, StyleSheet } from 'react-native';
import { MotiView } from 'moti';

import Text from './Text';
import colors from './colors';

export default ({ text, buttonText, onPress }) => (
  <MotiView style={styles.textLink}>
    <Text fontSize={12}>{text}</Text>
    <TouchableOpacity onPress={onPress}>
      <Text fontSize={12} color={colors.primary}>
        {buttonText}
      </Text>
    </TouchableOpacity>
  </MotiView>
);

const styles = StyleSheet.create({
  textLink: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
});
