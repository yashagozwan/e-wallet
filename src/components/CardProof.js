import React from 'react';
import { Image, StyleSheet, View } from 'react-native';
import moment from 'moment';

import currencyFormat from '../utils/currencyFormat';
import Text from './Text';
import IconBox from './IconBox';
import CenterHorizontal from './CenterHorizontal';
import Spacer from './Spacer';
import colors from './colors';

export default function CardProof({ name, accountNumber, amount, createdAt }) {
  return (
    <View>
      <Image
        style={styles.imageStyle}
        resizeMode="cover"
        source={require('../assets/image/cutting-top.png')}
      />
      <View style={styles.cardProof}>
        <CenterHorizontal>
          <IconBox
            typeLibrary="materialIcons"
            iconName="check"
            rounded={100}
            iconSize={50}
            width={90}
            height={90}
            mb={8}
          />

          <Text fontWeight={600} color={colors.primary}>
            Berhasil
          </Text>
        </CenterHorizontal>
        <Spacer bottom={32} />
        <Text fontSize={12}>Penerima</Text>
        <Text fontSize={14} fontWeight={600} mb={8}>
          {name}
        </Text>
        <View
          style={{
            borderBottomColor: colors.grey444,
            borderBottomWidth: 0.5,
            marginBottom: 8,
          }}
        />
        <Text fontSize={12}>Rekening</Text>
        <Text fontSize={14} fontWeight={600} mb={8}>
          {accountNumber}
        </Text>
        <View
          style={{
            borderBottomColor: colors.grey444,
            borderBottomWidth: 0.5,
            marginBottom: 8,
          }}
        />

        <Text fontSize={12}>Jumlah</Text>
        <Text fontSize={14} fontWeight={600} mb={8}>
          Rp {currencyFormat(parseInt(amount))}
        </Text>
        <View
          style={{
            borderBottomColor: colors.grey444,
            borderBottomWidth: 0.5,
            marginBottom: 8,
          }}
        />
        <Text fontSize={12}>Tanggal</Text>
        <Text fontSize={14} fontWeight={600} mb={8}>
          {moment(createdAt).format('LLL')}
        </Text>
        <View
          style={{
            borderBottomColor: colors.grey444,
            borderBottomWidth: 0.5,
            marginBottom: 8,
          }}
        />
      </View>

      <Image
        style={styles.imageStyle}
        resizeMode="cover"
        source={require('../assets/image/cutting-bottom.png')}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  imageStyle: {
    width: '100%',
  },
  cardProof: {
    backgroundColor: colors.white,
    padding: 32,
  },
});
