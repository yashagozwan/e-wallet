import React from 'react';
import { MotiView } from 'moti';

import Text from './Text';
import colors from './colors';
import Spacer from './Spacer';

export default ({ text }) => (
  <MotiView
    from={{ translateY: -25, opacity: 0 }}
    animate={{ translateY: 0, opacity: 1 }}
  >
    <Text center fontSize={13} color={colors.red}>
      {text}
    </Text>
    <Spacer />
  </MotiView>
);
