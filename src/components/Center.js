import React from 'react';
import { View, StyleSheet } from 'react-native';

export default ({ children, style }) => (
  <View style={[styles.center, style]}>{children}</View>
);

const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
