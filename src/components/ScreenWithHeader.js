import React from 'react';

import Screen from './Screen';
import Wrapper from './Wrapper';
import Header from './Header';
import Spacer from './Spacer';

const ScreenWithHeader = ({
  children,
  headerText,
  headerTextMarginbottom,
  bgcolor,
}) => (
  <Screen full bgColor={bgcolor}>
    <Wrapper>
      <Header headerText={headerText} />
    </Wrapper>
    <Spacer bottom={headerTextMarginbottom} />
    {children}
  </Screen>
);

export default ScreenWithHeader;
