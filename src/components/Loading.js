import React from 'react';
import { ActivityIndicator } from 'react-native';

import colors from './colors';
import Screen from './Screen';

export default () => (
  <Screen full style={{ alignItems: 'center', justifyContent: 'center' }}>
    <ActivityIndicator size="large" color={colors.primary} />
  </Screen>
);
