import React from 'react';
import { StyleSheet, View } from 'react-native';

export default ({ children, style, ph = 16 }) => (
  <View style={[styles.wrapper, { paddingHorizontal: ph }, style]}>
    {children}
  </View>
);

const styles = StyleSheet.create({ wrapper: { paddingHorizontal: 16 } });
