import React from 'react';
import {
  FontAwesome,
  FontAwesome5,
  MaterialCommunityIcons,
  Ionicons,
  MaterialIcons,
} from '@expo/vector-icons';

export default ({ name, size, color, style, typeLibrary }) => {
  switch (typeLibrary) {
    case 'fontAwesome':
      return (
        <FontAwesome name={name} size={size} color={color} style={style} />
      );
    case 'fontAwesome5':
      return (
        <FontAwesome5 name={name} size={size} color={color} style={style} />
      );
    case 'materialCommunity':
      return (
        <MaterialCommunityIcons
          name={name}
          size={size}
          color={color}
          style={style}
        />
      );
    case 'ion':
      return <Ionicons name={name} size={size} color={color} style={style} />;
    case 'materialIcons':
      return (
        <MaterialIcons name={name} size={size} color={color} style={style} />
      );

    default:
      return (
        <FontAwesome name={name} size={size} color={color} style={style} />
      );
  }
};
