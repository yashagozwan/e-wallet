export default {
  primary: '#677fff',
  primaryPressed: '#3a4aa0',
  white: '#fff',
  whiteBlue: '#e6e8e9',
  black: '#000',
  green: '#22B07D',
  grey: '#EEF2F8',
  red: '#EC4F3C',
  yellow: '#fcc404',
  grey333: '#333',
  grey444: '#444',
};
