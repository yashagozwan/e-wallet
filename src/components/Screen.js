import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { View } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';

import colors from './colors';

export default ({
  children,
  style,
  full = false,
  staturBarColor = 'auto',
  bgColor = colors.whiteBlue,
}) => (
  <SafeAreaView
    style={[{ flex: full ? 1 : 0, backgroundColor: bgColor }, style]}
  >
    <StatusBar style={staturBarColor} />
    <View style={{ marginBottom: 16 }} />
    {children}
  </SafeAreaView>
);
