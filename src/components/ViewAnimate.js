import React from 'react';
import { View } from 'moti';

export default ({ children, delay = 0 }) => (
  <View from={{ translateX: -200 }} animate={{ translateX: 0 }} delay={delay}>
    {children}
  </View>
);
